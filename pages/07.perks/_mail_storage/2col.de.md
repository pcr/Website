---
title: 'Email Speicher'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

<br>
In Deinem **Disroot** Account sind 1 GB FREIER Speicher für Deine E&#8209;Mails enthalten.

Natürlich ist es möglich, Deinen E&#8209;Mailspeicherplatz von 1 GB **auf bis zu 10 GB** zu erweitern, zum Preis von 0,15 EUR pro GB pro Monat mit jährlicher Zahlung (VAT nicht enthalten).

<a class="button button1" href="https://disroot.org/forms/extra-storage-mailbox">Zusätzlichen E&#8209;Mailspeicher beantragen</a>

---      

# E&#8209;Mailspeicher

![email](logo_email.png?resize=150)
