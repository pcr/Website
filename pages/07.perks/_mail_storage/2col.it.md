---
title: 'Spazio email'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

<br>
Con il tuo account **Disroot**, ricevi 1 GB di spazio di archiviazione gratuito per le tue e-mail. 

Tuttavia, è possibile estendere lo spazio di archiviazione della posta elettronica da 1GB **fino a 10GB** al costo di 0,15 euro per GB al mese, con pagamento annuale(IVA esclusa). 

<a class="button button1" href="https://disroot.org/forms/extra-storage-mailbox">Richiedi spazio extra per la tua Mailbox</a>

---      

# Spazio email

![email](logo_email.png?resize=150)
