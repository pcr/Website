---
title: 'Espace Cloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Stockage Cloud

![cloud](logo_cloud.png?resize=150)

---      

<br>
Avec votre compte **Disroot**, vous bénéficiez de 2 Go d'espace cloud GRATUIT.

Toutefois, il est possible d'étendre votre espace de stockage cloud de 2 Go **à 14, 29 ou 54 Go** pour le coût de 0,15 euro par Go par mois (hors TVA).

Vous pouvez également choisir d'utiliser une partie ou la totalité de l'espace supplémentaire pour le stockage des e-mails *(veillez à le préciser dans la section commentaire du formulaire de demande)*.

<a class="button button1" href="https://disroot.org/forms/extra-storage-space">Demande d'un espace cloud supplémentaire</a>
