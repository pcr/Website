---
title: 'Einführung'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;" id="fund1">
In den letzten Tagen dieses Jahres, während die ganze Welt sich darauf vorbereitet, das Licht der neuen Dekade zu begrüßen, kämpfen drei Domains um die Vorherrschaft auf dem Rübenmarkt. **Schließe Dich der Schlacht an und wähle Deinen Lehnsherrn**.
</span>

<span style="padding-right:30%;" id="fund2">
Nimm vom 23 Dezember bis zum 01. Januar an unserem Spendenevent des Jahresendes teil. **Spende an Disroot**, indem Du einen der unten aufgeführten Hashtags als Verwendungszweck angibst. Der siegreiche Domainname wird dann **jetzt und zukünftig als extra Alias an alle Disrooter vergeben**.
</span>

## Lasst die coolste Domain gewinnen!
