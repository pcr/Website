---
title: 'Rewards'
bgcolor: '#f2f2f2ff'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: "Doa 30€"
        text: "Recebe um postal de ano novo personalisado."
    -
        title: "Doa 75€"
        text: "Uma surpresa de ano novo a caminho da tua caixa de correio."
    -
        title: "Doa 100€"
        text: "Escolhe a nova imagem de cabeçalho do site disroot.org por duas semanas.*"
    -
        title: "Doa 500€"
        text: "Escolhe um novo domínio para um alias para ser fornecido a todos os utilizadores do Disrot.*"


---

<div markdown=1>
`*` Disroot.org reserva o direito de recusar uma imagem ou nome se estiverem em violação dos nossos <a href="/tos"> TOS</a>
</div>
