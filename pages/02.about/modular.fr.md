---
title: 'Nous'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about-who-and-why
            - _core-team
            - _federation
            - _privacy
            - _transparency
            - _greenenergy
body_classes: modular
header_image: about-banner.jpeg
---
