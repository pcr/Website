---
title: Transparency
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

<br>
![](about-trust_es.png)

---

# Transparencia y apertura

En **Disroot** utilizamos software 100% libre y de código de abierto. Esto significa que el [código fuente](https://es.wikipedia.org/wiki/C%C3%B3digo_fuente) (las instrucciones que hacen funcionar un software) es de acceso público. Cualquiera está en condiciones de contribuir con mejoras o ajustes al software y este puede ser auditado y controlado en cualquier momento, sin [puertas traseras](https://es.wikipedia.org/wiki/Puerta_trasera) ocultas u otro [malware](https://es.wikipedia.org/wiki/Malware) malintencionado.

Queremos ser completamente transparentes y abiertos con las personas que utilizan nuestros servicios y por ello publicamos regularmente información sobre el estado actual del proyecto, su condición financiera, nuestros planes e ideas. También nos gustaría escuchar sus sugerencias y comentarios así podemos dar la mejor experiencia posible a todxs lxs **Disrooters**.
