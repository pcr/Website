---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Disroot Team

Disroot è stato fondato da **Antilopa** e **Muppeth** nel 2015.
Nel luglio del 2019 **Fede** e **Meaz** si sono aggiunti al Team.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz")
