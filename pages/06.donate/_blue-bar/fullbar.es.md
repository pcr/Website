---
title: 'Blue bar'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## ¡Estamos agradecidos por sus donaciones y apoyo!

Con tu ayuda, estamos acercándonos a nuestra meta para alcanzar la sustentabilidad financiera, y probamos que un modelo social de economía es posible.
