---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Compartiendo nuestra buena fortuna'
        text: "Si recibimos al menos 400 EUR en donaciones, compartimos el 15% de nuestro excedente con lxs desarrolladores del software que utilizamos. Disroot no existiría sin ellxs."
        unlock: yes
    -
        title: 'Pagar una Cuota por Voluntariado'
        text: "Si nos quedan más de 150€ al final del mes, pagamos a un miembro del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: yes
    -
        title: 'Pagar dos Cuotas por Voluntariado'
        text: "Si nos quedan más de 300€ al final del mes, pagamos a dos miembros del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: yes
    -
        title: 'Pagar tres Cuotas por Voluntariado'
        text: "Si nos quedan más de 450€ al final del mes, pagamos a tres miembros del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: yes
    -
        title: 'Pagar cuatro Cuotas por Voluntariado'
        text: "Si nos quedan más de 600€ al final del mes, pagamos a cuatro miembros del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: no
    -
        title: 'Pagar cinco Cuotas por Voluntariado'
        text: "Si nos quedan más de 600€ al final del mes, pagamos a cinco miembros del Equipo Central de Disroot una cuota por voluntariado de 150€."
        unlock: no
---

<div class=goals markdown=1>

</div>
