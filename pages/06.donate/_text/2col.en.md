---
title: Goals
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

##  <span style="color:#8EB726;"> There's no such thing as a free service on the Internet! </span>

---

Whether it's the people giving their time to maintain the software or paying for hardware running it, the electricity used to power the machines or the hosting space in the data-center. With the big popular companies that give "free" service, it is most likely your personal user data that is paying the price. Imagine a scope of their operation, the amount of staff, and hardware needed to serve billions of users world-wide. They aren't charity, and yet provide "free" service...

Disroot.org is run by volunteers and offers all the services free of charge. We do not collect information to sell to advertising companies or government agencies. Thus we must rely on donations and support from users of the services and the community. If you would like to keep the project going and create space for potential new users, you can use any of the methods below to make a financial contribution.

**Remember, any amount of money counts! If all Disrooters would buy us a cup of coffee each month, we could run this service without any financial struggles.**
