---
title: Goals
bgcolor: '#FFF'
fontcolor: '#1F5C60'
wider_column: right
---

##  <span style="color:#8EB726;"> Es gibt keine kostenlosen Dienste im Internet! </span>

---

Sei es die Zeit, die Menschen für die Wartung von Software aufwenden, sei es der Preis für die Hardware auf der sie betrieben wird, sei es der Strom, der für den Betrieb notwendig ist, oder der Speicherplatz im Rechenzentrum. Bei den großen und populären Service-Providern, die ihre Dienste den Nutzern "kostenlos" anbieten, sind es mit hoher Wahrscheinlichkeit persönliche Daten, mit denen man für die Nutzung zahlt. Stell dir nur den Umfang der operativen Arbeit, die Anzahl an Mitarbeitern und die Hardware vor, die benötigt werden, um einen Service mit Milliarden von Nutzern weltweit zu betreiben. Das sind keine Wohltätigkeitsbetriebe und sie bieten auch keine "kostenlose" Dienste an...

Disroot.org wird von freiwilligen Helfern betrieben und bietet alle Dienste kostenlos an. Wir sammeln keine Nutzerdaten, um sie an Werbefirmen oder Regierungsbehörden zu verkaufen. Daher sind wir auf Spenden und die Unterstützung von unseren Nutzern und Mitgliedern der Gemeinschaft angewiesen. Wenn Du das Projekt am Laufen halten und die Möglichkeit für potenzielle neue Nutzer schaffen möchtest, kannst du eine der unten aufgeführten Methoden nutzen, um uns mit einen finanziellen Beitrag zu unterstützen.

**Jeder Beitrag zählt! Wenn uns alle Disroot-Nutzer einen Kaffee pro Monat spenden würden, könnten wir unseren Service ohne finanzielle Sorgen anbieten**
