---
title: Leitbild
bgcolor: '#FFF'
fontcolor: '#1F5C60'
---

---

### Die praktischen Dinge des Lebens

Um das Disroot.org-Projekt in Übereinstimmung mit den oben genannten Prinzipien arbeiten zu lassen, operieren wir entsprechend der folgenden praktischen Prinzipien:

##### 1) Finanzierung

Disroot.org ist eine gemeinnützige Stiftung und alle Einnahmen der Disroot-Plattform werden in das Projekt reinvestiert, in die Wartung der Services und in die Entwicklung freier Software.

Es gibt verschiedene Wege der Spenden für das Projekt und seine Entwicklung.

A. Spenden von Nutzern
<br>
Wir sind der Meinung, dass die Auszeichnung unserer Services mit Preisen in gewisser Weise die Autonomie der Nutzer einschränkt. Daher legen wir diese Verantwortung vertrauensvoll in die Hände der Disrooter. Nach unserem Verständnis variiert der Wert des Geldes abhängig von verschiedenen Faktoren wie z.B. geographische Lage, wirtschaftliche oder persönliche finanzielle Situation. Also haben wir entschieden, die Disrooter selbst einschätzen zu lassen, wie viel sie zum Projekt beitragen können.
<br>
Es ist uns wichtig die Tatsache zu betonen, dass nichts im Internet kostenlos ist und Online-Dienste immer Kosten verursachen, für die mit echtem Geld oder wertvollen persönlichen Daten bezahlt wird. Wir sind uns sicher, dass die Freiheit des Internet nur dann erreicht werden kann, wenn die Menschen dies erkennen und freiwillig ihren Beitrag leisten. Da Disroot.org keine Nutzerdaten verkauft, sind wir hochgradig abhängig von den Spenden unserer Nutzer.

B. Förderungen oder Subventionen
<br>
Die Angebote von Fördergeldern, Großspenden oder ähnlicher Formen der Unterstützung werden nur akzeptiert, wenn sie in keiner Weise die Unabhängigkeit des Projekts gefährden. Wir werden niemals die Privatsphäre der Disrooter oder unsere Freiheit, die Services auf unsere Weise zu verwalten, aufs Spiel setzen, nur um die Anforderungen von "Spendern" zu erfüllen. Uns ist klar, dass Fördergelder meistens mit einem bestimmten Zweck oder für ein gewünschtes Ergebnis zugeteilt werden, aber wir sind auch davon überzeugt, dass einige dieser Anforderungen erfüllt werden können, ohne dem Projekt oder dem unabhängigen, föderierten Netzwerk im Ganzen zu schaden. Wir werden jede mögliche Förderung sorgfältig prüfen, bevor wir sie akzeptieren, um sicherzustellen, dass sie nicht gegen die grundlegenden Prinzipien des Projekts verstößt.

##### 2) Entscheidungsprozess

Unsere Entscheidungsprozesse basieren auf dem Prinzip des Konsens. Innerhalb des Disroot-Kernteams hat jeder eine gleichberechtigte Stimme und eine gleichberechtigte Meinung. Alle wichtigen Entscheidungen werden in Meetings getroffen, in denen Vorschläge eingebracht und ausführlich diskutiert werden, sodass jeder ausreichend Zeit hat, sich eine Meinung zu bilden oder Bedenken vorzubringen, bevor es zu einer einvernehmlichen Entscheidung kommt.

##### 3) Einbeziehung der Gemeinschaft

Disroot.org wäre sinnlos ohne die Gemeinschaft, die das Projekt umgibt und deren aktive Teilnahme am Projekt sehr wichtig und gleichermaßen engagiert ist. Wir begrüßen jede Hilfe, Verbesserung, Rückmeldung und jeden Vorschlag und bieten verschiedene Möglichkeiten der Kommunikation zwischen den Mitgliedern der Gemeinschaft und dem Disroot-Kernteam, wie zum Beispiel: das Forum, verschiedene Taiga-Projektboards oder Instant Messaging Räume.
