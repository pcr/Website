---
title: 'Term Of Services'
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: tos
text_align: left
---

<br>
**v1.2 - March 2020**

<a name="about"></a>
<br>
## About this document

This document has been originally written in English and is the only version for which **Stichting Disroot.org** can be held accountable.<br>
Any translation of this **Terms of Services** is a community effort to make the information accessible in other languages and should be taken as such, with no other value than merely informative.
<br>
# Accepting these Terms
By using any of the services provided by **Disroot.org** you agree to be bound by our Terms of Services. So, you should start reading these terms before first using our platform.

## Community Notice
**Disroot** is a community and our platform a part of the Free Software and Open Source ecosystem. We all share servers resources, collaborate and use **Disroot.org** services together. It is very important that each and everyone of us understands it and feels equally responsible for it. So, please keep to the following guidelines so we all get along.

# Terms of Services
<a name="users"></a>
<br>
## 1. Who can use our services?
Any person who is at least 16 years of age can use the Services.

<br>

<a name="space"></a>
<br>
## 2. Space limits on disk
**Resources are scarce**. Have in mind that keeping unnecessary files and emails might limit server space, which can lead to less accounts served to others.<br>**Don't litter!**

<br>

<a name="expiration"></a>
<br>
## 3. Account expiration
For safety reasons, **Disroot.org** does not expire unused accounts. This might be a subject to change if we are forced to save up disk space for other users. Such changes in our terms will be announced via all communication channels well in advance.<br>We would rather appreciate people taking responsibility for their account and its impact on **Disroot** resources, and rather [**delete their account**](https://user.disroot.org) when they decide not to use our services any longer, or purge unused files.

<br>

<a name="password"></a>
<br>
## 4. Password
We never keep track of anybody who requests the activation of a **Disroot** account. When creating your account, you may either choose security questions and/or adding a secondary email, both of which will allow you to recover your password in case you lose/forget it.<br>
- If you don't set a secondary email or forget the question or the answers to the questions, we won't be able to re-send you the data to access your account again. Remember as well to periodically change your password: you can do it by login into the [User Self-Service](https://user.disroot.org) and clicking the 'Change password' button.<br>

**! WARNING !** Lost passwords will lead to un-recoverable cloud file loss. To protect yourself from it, you can enable password recovery feature in your personal Cloud settings.

<br>

<a name="spam"></a>
<br>
## 5. SPAM and Viruses
To avoid having our servers included in blacklists:
- We do not allow the use of Disroot email accounts to send spam. Any account caught doing so will be disabled without further notice.

- If you happen to receive great amount of SPAM or any suspicious emails please let us know at support@disroot.org so we can investigate it further.

<br>


<a name="copyright"></a>
<br>
## 6. Copyright material
One of the main aims of **Disroot** is to promote Free Libre and Open Source software and all kind of copyleft content. Embrace it and support artists and projects that release their work under those licences.
- Uploading and (most of all) publicly sharing copyrighted material on **Disroot** could put other users and **Disroot.org** in serious legal troubles as well as endanger the whole project and the existence of our servers (and all services connected to it) so **just don't do it**.

<br>


<a name="not_allowed"></a>
<br>
## 7. Activities not allowed
Do not engage in the following activities through the services provided by **Disroot.org**:

<ol class=disc>
<li> <b>Harassing and abusing others</b> by engaging in threats, stalking, or sending spam.</li>
<li> <b>Contributing to the discrimination, harassment or harm against any individual or group.</b> That includes the spread of hate and bigotry through racism, ethnophobia, antisemitism, sexism, homophobia and any other forms of discriminatory behavior.</li>
<li> <b>Contributing to the abuse of others</b> by distributing material where the production process created violence or sexual assault against persons or animals.</li>
<li> <b>Impersonating or portraying another person</b> in a confusing or deceptive manner unless the account is clearly classified as a parody.</li>
<li> <b>Doxing</b>: the publication of other people's personal data is not allowed because:<br> (I) we cannot assess either the purposes or what consequences it will have for the exposed people, and (II), it implies too big a legal risk for <b>Disroot.org</b>. </li>
<li> <b>Misuse of services</b> by distributing viruses or malware, engaging in a denial of service attack, or attempting to gain unauthorized access to any computer system, including this one.</li>
<li> <b>Activities not permitted</b> by national or international law.</li>
</ol>

<br>

<a name="commercial"></a>
<br>
## 8. Using Disroot services for commercial activities
**Disroot** is a non-profit organization providing services for individuals on "pay as you wish" basis. Because of this structure we see using **Disroot** services for commercial purposes as an abuse of the service and it will be treated as such.
These commercial activities or purposes include, but are not limited to:
<ol class=disc>
<li>Trading any <b>Disroot</b> services with a third party.<br>This is prohibited.</li>
<li>Using email for purposes such as "no-reply" type of accounts for a business.<br>If such activity is detected, the account will be blocked without a warning.</li>
<li>Sending bulk emails, including but not limited to marketing and advertising, for business purposes.<br>
The account will be treated as spam and blocked upon discovery without any prior notice.</li>
<li>Using <b>Disroot</b> services for financial gain, including but not limited to trading or managing sales, is not tolerated.<br>Accounts created for the purpose of generating profits will be subject to termination upon inquiry.</li>
</ol>
<br>
Using <b>Disroot</b> services for any other commercial activity will be examined per case and the decision on terminating such accounts will be based upon communication with the account holder and the type of the activities in question.
<br>
If you have any doubts or questions about the scope of the term "commercial activities", please contact us.

<br>

<a name="ownership"></a>
<br>
## 9. Ownership of and responsibility for content
**Disroot.org** is not responsible for any content published on any of our services. You are responsible for your use of the **Disroot**'s services, for any content you provide and for any consequences thereof. However, to secure our existence, we reserve the right to remove any content if found in violation with applicable law.

<br>

<a name="legal"></a>
<br>
## 10. Legal responsibilities
As for anything else you do, be aware that **Disroot.org** is not responsible for what you write, nor for the safeguard of your own privacy. We therefore invite you to make the most of all the existing tools to defend your rights.<br><br>
To learn more about how your information is being stored and used, please refer to our **Privacy Statement**.

<br>

<a name="laws"></a>
<br>
## 11. Laws
**Disroot.org** is hosted in the **Netherlands** and therefore is subject to **Dutch** laws and jurisdiction.

<br>

<a name="termination"></a>
<br>
## 12. Account termination
**Disroot.org** may terminate your service at any time under the following conditions:
<ol class=disc>
<li>The account has been found to be sending SPAM (excessive amounts of unsolicited email).</li>
<li>The account has engaged in one or more of the banned activities listed above.</li>
</ol>
The above conditions are subject to change at any time.

<br>


<a name="warranty"></a>
<br>
## 13. No Warranty
You understand and agree that **Disroot.org** provides you primarily with internet-based services. Therefore, services are offered as is, subject to availability and without liability to you. As much as we would not like to let any of our users down, we just can  not give any warranty as to the reliability, accessibility, or quality of our services.<br> You agree that the use of our services is at your sole and exclusive risk.

<br>


<a name="changes"></a>
<br>
## 14. Changes on these Terms
We reserve the right to modify these conditions at any time. We highly evaluate transparency and do our best to make sure users are notified as soon as possible about any major changes via email, social networks and/or on our [blog](https://disroot.org/en/blog). Minor changes will most likely only be published on our blog.
<br>
You can follow the history of changes on this document on our git repository [**here**](https://git.disroot.org/Disroot/Disroot-ToS/commits/branch/master)


[Back to top](#top)
