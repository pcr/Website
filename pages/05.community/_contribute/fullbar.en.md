---
title: Contribute
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)
# How can I help?

There are many ways to become active and contribute to the **Disroot** project.
