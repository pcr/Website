---
title: '¡Nuevo webmail!'
media_order: roundcube.png
published: true
date: '20-04-2020 00:30'
taxonomy:
    category:
        - noticias
    tag:
        - roundcube
        - webmail
        - noticias
body_classes: 'single single-post'

---
Queridxs Disrooters:

A partir del 8 de junio, reemplazaremos el actual webmail (desarrollado por **Rainloop**) por un nuevo juguete creado por **Roundcube**.

Durante los últimos cuatro años hemos estado utilizando **Rainloop** para nuestra experiencia con el webmail. Pero, debido a algunas características ausentes y a una necesidad de mejorar la integración con la plataforma, decidimos re introducir un entorno de webmail construido sobre **Roundcube**.

**Roundcube** ha sido utilizado ampliamente durante los últimos diez años y proporciona la solución moderna, rápida, simple y confiable que unx podría esperar. Aquí algunas características destacadas:

* Tema de Disroot con buena adaptación (y modo oscuro)
* Mejor soporte de filtros
* Archivado de correo y bandeja de entrada
* Temas personalizados
* La interfaz está traducida a más de ochenta idiomas.
* Gestión de mensajes arrastrar-&-soltar: mueve tus mensajes con el ratón.
* Soporte completo para mensajes MIME y HTML.
* Vista previa de adjuntos: ve las imágenes directamente dentro de tus mensajes.
* Lista de mensajes enlazados: muestra las conversaciones en modo de hilos.
* Carpetas: gestiona tus carpetas (agregar, quitar o esconder).
* Autenticación de dos factores: agrega un segundo nivel de autenticación a tu inicio de sesión.

*  Y mucho más...

Para el futuro próximo estamos planeando integrarlo con el cifrado del buzón además de con **Nextcloud** (calendarios, contactos y adjuntos) y otros servicios ofrecidos actualmente por **Disroot**. Pensamos que **Roundcube** nos permitirá mejorar sus experiencias con el correo electrónico.

Esperamos que les guste el nuevo look y las nuevas características. Nosotrxs, seguro, estamos esperando este cambio y las futuras mejoras.

**!! ¡Aviso a lxs usuarixs de 2FA!**

Aquellxs de ustedes que tienen la 2FA habilitada en el actual webmail, tendrán que re-configurarla de nuevo una vez que el nuevo esté funcionando. ¡Disculpen por las molestias!
