---
title: 'Using Nextcloud Deck'
media_order: deck_web_interface.jpg
published: true
date: '12-09-2020 00:00'
taxonomy:
    category:
        - news
    tag:
        - nextcloud
        - deck
        - tutorial
        - users experiences
body_classes: 'single single-post'
---

**Hello Disrooters**:

Since I've been using **Nextcloud Deck** privately for several months now, and most serious bugs have been fixed (there were some problems interacting with the quite new **Android** app), I think it’s a good moment to give the Community a short overview.

---

# What is Deck?
**Nextcloud Deck** is a **KANBAN** style organization tool. So to begin with, what is KANBAN?

Its origins come from the process control of production in industry and it was developed by **Toyota** in **1947** to increase the insufficient productivity of the company. It is basically based on lists in which tasks are placed as cards.

While agile methods are becoming more and more common in software development, KANBAN is still the absolute standard method in all production facilities around the world.

## How does it look like?

![](en/deck_web_interface.jpg)

## What features does it provide?

* Add your tasks to cards and put them in order
* Assign user to cards
* Assign labels for even better organization
* Write down additional notes in markdown
* Share with your team, friends or family
* Attach files and embed them in your markdown description
* Discuss with your team using comments
* Keep track of changes in the activity stream

# Getting started

In **Disroot**’s default setup the **Deck** app isn't present in the app navigation bar.

![](en/navbar.png)

To add it, click on your profile picture on the top right corner and choose “Settings”.<br>
Then go to “App order” and tick the **Deck** checkbox.

![](en/app_order.gif)

Tip of the day: take the opportunity to change the app order to your needs via drag'n'drop.

![](en/navbar_deck.png)

Here is a tutorial that shows the most important functions of **Deck**.<br>
- [Deck functions tutorial](https://deck.readthedocs.io/en/latest/User_documentation_en/) (The UI is from an older Nextcloud version, but nevertheless it's helpful.)

## Android companion app

With the **Android** app you can access and organize your projects even when you are on the road.<br>
It offers almost all functions that are available in the web interface.

![](en/deck_android_app.jpg)

**Get it on**

![https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/](en/f-droid.png)


## What's missing?

Not much actually. For larger and more complex projects, the possibility to tweak notification setting more specifically would of course be desirable.<br>
  - [Ability to choose to be notified per board](https://github.com/nextcloud/deck/issues/713)<br>

It would also be great if cards with a due date were shown in the calendar.<br>
  - [Integrate with Nextcloud CalDAV API](https://github.com/nextcloud/deck/issues/15)


## Final words

I've been using **Deck** for half a year now to coordinate smaller projects and tasks in my daily life. This works very well for me and this app is definitely worth a look.

**Best regards,<br>
avg_joe**


### Sources and links:

- [Nextcloud Apps](https://apps.nextcloud.com/apps/deck)
- [Nextcloud Documentation](https://deck.readthedocs.io/en/latest/User_documentation_en/)
- [Nextcloud Deck Source code](https://github.com/nextcloud/deck)
- [F-Droid Android App](https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/)
- [KANBAN on **Wikipedia**](https://en.wikipedia.org/wiki/Kanban)
