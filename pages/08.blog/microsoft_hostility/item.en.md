---
title: 'Microsoft hostility'
media_order: power.jpg
published: true
date: '7-11-2020 12:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - microsoft
        - email
        - news
body_classes: 'single single-post'
---

**UPDATE!**
*As of beginning of december 2020, after hundreds of requests we have been finnaly delisted and so email flow to Microsoft owned email services has been restored!.*
<br><br>
As some of you might have noticed, **Microsoft** (owner of Hotmail, Outlook, Live, Office360, etc.) is rejecting all emails originating from **Disroot.org** servers. Once reported to us, we immediately contacted support at **Outlook** as it was not the first time we have been targeted by **Microsoft** (last year for no reason, all emails from **Disroot** were marked as spam).

After waiting for a reply for a few days, making sure we are compliant with 'their policies' and exchanging few pointless emails with the **Microsoft**'s support staff, we have gotten this final reply:

> Hello,
> As previously stated, your IP (178.21.23.139) do not qualify for mitigation at this time. I do apologize, but I am unable to provide any details about this situation since we do not have the liberty to discuss the nature of the block.
> At this point, I would suggest that you review and comply with Outlook.com's technical standards. This information can be found at https://postmaster.live.com/pm/postmaster.aspx.
>  We regret that we are unable to provide any additional information or assistance at this time.
>  Best regards"

This means as much as "*You are blocked, we won't tell you why, and all you can do is wait for us to decide otherwise and allow your emails to arrive*". Good ol' Microsoft hostility.

**We are sorry** that your emails are not reaching your contacts at **Hotmail**, **Outlook** or **Live**. Searching the web for possible reasons we have learned that it is very common to be banned by MS without any reason for extended periods of time.

Perhaps if their users start asking questions, **Microsoft** will finally review the reasons they have blocked us for - and maybe be so kind to explain it to us - or simply unblock. So **contact Microsoft!**. However, you'll discover how hard it to get in touch with them! One way though is through this page:

[https://support.microsoft.com/en-us/contactus/](https://support.microsoft.com/en-us/contactus/)

**Please, take a stand, get involved, repost and share so we can together get Microsoft's attention and undo this block.**
