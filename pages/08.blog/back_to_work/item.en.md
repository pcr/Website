---
title: "Let's get back to work"
date: '04-09-2019 11:30'
media_order: coffee.jpg
taxonomy:
  category: news
  tag: [disroot, news, sprint, translations, conversejs, hubzilla]
body_classes: 'single single-post'

---

*(Sprint 43)*

Hi there. Well rested? Ate nice food, visited places? Nice. Now get back to work!

Past weeks, as we wrote in the previous blog posts, were full of work. To the point we realized we are taking way too much on our shoulders, which means we tend to start things but not finish them on time. Now, slowly we are getting around finishing previously started things so here is a small dump of things we recently been busy with. Ahhh, and don't worry we did have some leisure time too.

# New Translations

In the past weeks we tested various translation softwares but couldn't find anything that meet our requirements. Websites and documentation, unlike applications, are much harder to translate using dedicated software and require lots of workarounds for very little improvement. So we decided to continue the translation work through **Git**. It might create a higher entry level to some but learning the basics of **Git** and **Markdown** are good skills to have.

So, we’ve started to implement a new procedure to create, edit and translate the howtos and tutorials. We've also created a very nice overview of the current state of translations, a README file for the **Howto** repository and a **Taiga** board where even multiple people working on the same language can coordinate their work. So far, it works very well. We’re still making adjustments, refining the documentation and improving the process, but we’re in the right direction to have and provide all the guides and tutorials in as many languages as possible. You can check the **Howto Translations State** [here](https://howto.disroot.org/en/contribute/translations_procedure/state/Translations_Howto.pdf) and the **Website Translations State** [here](https://howto.disroot.org/en/contribute/translations_procedure/state/Translations_Website.pdf) (they already needs an update thanks to all you submitting translations).

In the last few weeks we’ve started translating the site and some howtos into **Russian** and **German**, and revisions and updates into **French**, **Italian** and **Spanish**. The biggest credit for this progress goes to the disrooters **shadowsword**, **wisbit, l3o** and **keyfear**, for getting involved so much and contributing not only with the translations, but also through feedback and collaborative approach.

**Fede** has been a busy ant coordinating all the work as well merging, verifying and approving all the changes and translations. He's been working on improvements to procedures and overviews.

If you want to get in touch with the team and help out with translations or the howtos, join our XMPP room at: [howto@chat.disroot.org](https://webchat.disroot.org/#converse/room?jid=howto@chat.disroot.org)<br>

 - [Translations board](https://board.disroot.org/project/fede-disroot-translations/epics)
 - [Howto board](https://board.disroot.org/project/disroot-disroot-h2/epics)

# New howto theme

In the past weeks **Antilopa** has been busy creating a new theme for [https://howto.disroot.org](https://howto.disroot.org). A while back we noticed that the old theme was not suitable to our needs. The information became harder to find as the number of tutorials grew. The effect as always, is pretty great. **Antilopa** did a very nice, minimalistic and clean theme that resembles our website's look and feel. There is still some issues we have been informed about regarding how the new site renders on some screens and **Antilopa** is working to fix it. If you find anything, please report it on our [issue board](https://board.disroot.org/project/disroot-disroot/issues). Remember to add *[WEBSITE]* prefix in the issue title so it can be hunted faster and fixed.

# Working out some internal procedures

**Meaz** has been busy going through our support ticketing queue which has become messy like a student's room after a week long house party. The result of this is an improved, clearer support queues, as well as introduction of shifts. Every week a different person is responsible to assign tickets, keep track of whats incoming as well as kick the ass of those who slack too much with replies (wink wink you know who you are...).
<br><br>

**Fede** drafted a nice welcome document that we will be putting somewhere on the website shortly. It should help onbording of new users. We have also started long discussion about things we can improve to make new users easily find their home. We know there are a lot of rough edges everywhere. Our goal is to work on more unified experience across all services. We have spent some time brainstorming and defining some of the low hanging fruit and we'll get to the implementation in the coming sprints.

# FAQ

Yes finally. No more answering *'use just username and not username@disroot.org'* for the hundredth time each day :P Thanks to **Meaz**, we have finally started and compiled a **FAQ** on the website **\o/**. Additionally, he also started to teach our bot **milkman** (name is subject to change, the joke ain't funny anymore), to answer the **FAQ** questions for all those on the chat side. **Antilopa** will be still working on visual and UX improvements of the page and of course we will be adding more questions and even more answers as we go.

# Red Pill
![](redpill_0.png)

**RedPill** is our take on **XMPP** webchat from **Opcode** called **ConverseJS**. Last weeks **Muppeth** as a side project decided to work on the looks of the webchat. Result of which you can observe at [https://webchat.disroot.org](https://webchat.disroot.org). This is just the beginning and long awaited change. In near future we will be busy implementing various features like audio/video conferencing, voice messages, reactions, as well as many visual improvements. This is just the first take on it. The name for our flavor of the client was a last minute idea, hence the main and dominating color is blue instead of red but we will be checking into how reds will work out in this setting. For those who want to follow the work on the webchat, you can use [https://devchat.disroot.org](https://devchat.disroot.org) where we play and test ideas. Source code of our changes will be published as soon as we are finished migrating our current git repositories to a new home.

Here you have some screenshots:
![](redpill_1.png)

![](redpill_2.png)

# Diaspora addon on Hubzilla

...And time for bad news. Despite spending countless hours debugging issues on our **Hubzilla** connector to the **Diaspora** network, we have not made any progress in terms of finding a solution. Currently (as is for months in fact) **Diaspora** addon is disabled. We have for now decided to not spend any more resources actively searching for solutions. We will however continue to report back to the **Hubzilla** main devs. We hope in the future an issue will be spotted, if not by us then perhaps someone else and fixed. We are now going to focus instead on getting **Hubzilla** to the point where is goes out from our open beta testing and gets it's place on the website as the goto social network.

<br><br>
As you can see this was quite some stuff. Don't worry next posts will be much shorter as we are now very strict on the amount of work we take on :P
