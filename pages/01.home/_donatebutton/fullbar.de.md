---
title: Donate button
fontcolor: '#FFF'
bgcolor: '#fff'
text_align: center
padding: 20px
body_classes: modular
---

<br>
<a class="button button2" href="https://disroot.org/de/donate">Unterstütze uns</a>
