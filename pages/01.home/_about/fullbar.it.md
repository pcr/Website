---
title: Disroot is
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot è una piattaforma che fornisce servizi online basati sui principi di libertà, riservatezza, federazione e decentralizzazione.
**Nessun tracciamento, nessuna pubblicità, nessuna profilazione, nessun data mining!**
