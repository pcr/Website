---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "ENCRYPTION"
section_number: "200"
faq:
    -
        question: "Can I use 2FA?"
        answer: "<p>Yes, you can use it for the <b>Cloud</b>. But, before you enable it, make sure you fully understand how it works and how to use it. For more information, go <a href='https://howto.disroot.org/en/tutorials/cloud/settings#two-factor-authentication' target='_blank'>here</a></p>"
    -
        question: "Can I use end-to-end encryption in the cloud?"
        answer: "<p>Currently, end-to-end encryption is disabled due to long standing bug with <b>Nextcloud</b> desktop app.</p>"


---
