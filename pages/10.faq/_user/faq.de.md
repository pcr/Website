---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "BENUTZER"
section_number: "500"
faq:
    -
        question: "Ich habe mein Passwort vergessen. Wie kann ich es zurücksetzen?"
        answer: "<p>Um Dein Passwort zurückzusetzen musst Du Sicherheitsfragen und/oder eine Wiederherstellungs-Emailadresse eingerichtet haben. Wenn das der Fall ist, gehe in die <a href='https://user.disroot.org/' target='_blank'>Benutzerverwaltung </a> und wähle die Option 'Passwort vergessen'.</p>
        <p>
        Wenn Du keine dieser Optionen eingerichtet hast, kannst Du nichts anderes tun als einen neuen Account einzurichten.</p>"
    -
        question: "Mein Account wurde bestätigt, aber ich kann mich nicht anmelden. Warum?"
        answer: "<p>In den meisten Fällen ist es so, dass Du:</p>
        <ul class=disc>
        <li>Deinen Benutzernamen (denk dran, nur der Benutzername, ohne die Domain), oder...</li>
        <li>Dein Passwort (denk an Groß- und Kleinschreibung) nicht korrekt eingibst.</li>
        </ul>"
    -
        question: "Ich kriege die Nachricht <i>'Invalid private key for encryption app'</i>. Warum?"
        answer: "<p>Wahrscheinlich hast Du kürzlich Dein Passwort geändert. Da Nextcloud das Benutzerpasswort nutzt, um die Verschlüsselungsschlüssel zu generieren, ist es notwendig, diese aus dem neuen Passwort neu zu generieren. Um das zu tun, gehe folgendermaßen vor:</p>
        <ol>
        <li>melde Dich in der Cloud an; </li>
        <li>klicke auf Dein Avatarbild; </li>
        <li>wähle Einstellungen aus dem Menü;</li>
        <li>wähle <b>Sicherheit</b> in dem '3-Balken-Menü'' </li>
        <li>scrolle runter bis zum Eintrag <b>Basisverschlüsselungsmodul</b> und gib Dein altes Passwort ein; </li>
        <li>dann das neue; </li>
        <li>bestätige die Eingabe></li></ol>

        <p>Nach einmal Ab- und wieder Anmelden in die Cloud solltest Du Deine Dateien wieder sehen und die Mitteilung sollte verschwunden sein.</p>

        <p>Wenn Du Dich nicht mehr an Dein altes Passwort erinnerst, ist es immer noch möglich, Deinen Account zurückzusetzen. Es ist jedoch nicht möglich, Deine Dateien zu entschlüsseln, da diese mit dem alten Schlüssel verschlüsselt wurden.</p>"
    -
        question: "Verfällt mein Disroot-Account, wenn ich ihn länger nicht benutze?"
        answer: "<p> Nein. Wir lassen ungenutzte Accounts nicht verfallen.
        Dennoch begrüßen wir es, wenn unsere Nutzer Verantwortung für ihren Account und dessen Einfluss auf die <b>Disroot</b>-Ressourcen übernehmen, indem sie eine Löschung des Accounts beantragen, wenn sie ihn nicht länger nutzen wollen.</p>"
    -
        question: "Wie kann ich meinen Disroot-Account löschen?"
        answer: "<p>Um Deinen <b>Disroot</b>-Account zu löschen, melde Dich unter <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> an und wähle <b>'Account löschen'</b>.</p>
        <p>Beantragte Löschungen werden täglich durchgeführt.</p>"
    -
        question: "Wie kann ich mein Passwort ändern?"
        answer: "<p>Melde Dich unter <a href='https://user.disroot.org' target='_blank'>https://user.disroot.org</a> an und wähle <b>'Passwort ändern'</b>.</p>
        <p>Denk daran, auch Dein <b>Nextcloud</b>-Passwort zu ändern, um weiterhin Zugriff auf Deine Dateien, die mit Hilfe Deines Passworts verschlüsselt wurden, zu haben. Um das zu tun, befolge die Schritte, die in der Antwort zu 'Ich kriege die Nachricht <i>'I get an 'Invalid private key for encryption app' Warum?'</i> beschrieben sind oder schau in diesem <a href='https://howto.disroot.org/en/user/account/ussc#change-your-password' target='_blank'>tutorial.</a></p>"

---
