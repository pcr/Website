---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "ALLGEMEIN"
section_number: "300"
faq:
    -
        question: "Was heißt 'Disroot'?"
        answer: "<p><b>Dis'root'</b>: Entwurzeln oder bei der Wurzel packen; folglich die Grundfesten erschüttern.</p>"
    -
        question: "Wird Disroot bestehen bleiben?"
        answer: "<p>Die Administratoren und das Kernteam von <b>Disroot</b> nutzen es täglich und verlassen sich darauf als ihr Hauptkommunikationsmittel. Wir beabsichtigen, es für eine lange Zeit am laufen zu halten. <a href='https://forum.disroot.org/t/will-disroot-last/101' target='_blank'>Schau ins Forum für die lange Antwort</a></p>"
    -
        question: "Wie viele Menschen nutzen Disroot?"
        answer: "<p>Wir überwachen nicht, welche Nutzer aktiv sind, daher können wir diese Frage nicht beantworten. Davon abgesehen sind wir der Auffassung, dass dies in keiner Weise ein Maßstab für den Status der Plattform ist. Nutzerzahlen sind anfällig für Manipulationen und werden gerne aus verschiedenen Gründen manipuliert, z.B. für den 'Wow'-Faktor oder um Investoren zufrieden zu stellen. Da die Zwecke dieser Plattform nichts mit so etwas zu tun haben, sehen wir keinen Grund, irgendwelche potentiell falschen oder manipulierten Statistiken zu veröffentlichen.</p>"

---
