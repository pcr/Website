---
title: Faircoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

![](faircoin-qr.png)

---

# Faircoin
Envía tus [faircoins](https://es.wikipedia.org/wiki/Faircoin) a nuestra cartera a:  
fLtQUZWMBiYkEneKb1hqTDoyoqzCkjL2qY
