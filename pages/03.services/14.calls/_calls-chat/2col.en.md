---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Use a text chat along side your audio/video conference.

---

![](chat.png)
