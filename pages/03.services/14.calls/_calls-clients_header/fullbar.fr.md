---
title: 'Jitsi Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

# Utilisez votre application préférée
Vous pouvez choisir parmi des clients de bureau, Web ou mobiles. Choisissez celui qui vous plaît le plus.
