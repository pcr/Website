---
title: Taiga
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org">Registrati su Disroot</a>
<a class="button button1" href="https://board.disroot.org/">Accedi</a>

---
![taiga_logo](taiga.png)

## Project management board


Taiga è uno strumento di project management sviluppato per programmatori, progettisti e startup che lavorano secondo la metologia AGILE. Può tuttavia essere applicato praticamente a qualsiasi progetto o gruppo, anche al di fuori dell'ambito IT.
Crea una panoramica chiara e visiva dello stato attuale del tuo progetto a chiunque sia coinvolto. Rende la pianificazione molto semplice e mantiene voi e il team concentrati sui compiti. Taiga può essere adattato a qualsiasi tipo di progetto grazie alla sua personalizzazione: da complessi progetti di sviluppo software a semplici lavori domestici. Il limite è la vostra immaginazione.

Se non avete mai usato uno strumento simile, sarete sorpresi di come la vostra vita può essere migliorata con Taiga. Crea semplicemente un progetto, invita i membri del tuo gruppo, crea i compiti e mettili sulla lavagna. Decidere chi si assumerà la responsabilità dei compiti, seguire i progressi, commentare, decidere e vedere il vostro progetto prosperare.
