---
title: 'Mumble Features'
wider_column: left
---

## Easy to use

Download the Mumble app available on your device (desktop, Android or iOS), connect to [mumble.disroot.org](https://mumble.disroot.org/) and you can already start creating an audio channel and talk to people!

## Great audio quality

Low-latency and noise suppression, so it is great for talking. You can have about 100 simultaneous participants!

## Private and secure

Communications are encrypted, and authentication is by public/private-key by default. If you use the Desktop Mumble software, you can give very specific permissions to your channels, groups and users.

## Mobile app

Manage your project on the go with Android mobile app (available on F-Droid) or iOS app.


---

![](en/connected.png?lightbox=1024)

![](en/notification.png?lightbox=1024)

![](en/acl.png?lightbox=1024)
