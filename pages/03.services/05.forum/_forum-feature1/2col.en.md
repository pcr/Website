---
title: 'Feature 1'
wider_column: right
---

![](en/discourse_feature1.png?lightbox=1024)

---

## Mailing list and forum in one
Next to a full functioning forum, Discourse can be also used as a mailing-list. You can start new topics and reply by sending an email. There is no need to even login to the web interface, you can use it just like traditional mailinglist

## Mobile friendly interface
With a built in mobile layout, you don't need an external app on your mobile.

## Public vs. private groups
You can start public or private forum groups that are self moderated. As a group administrator you will be able to manage who will participate in your forum group.
