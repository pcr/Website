---
title: 'Xmpp Encrypt'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Cifra tutto!

Utilizzando un metodo di cifratura End-to-End come OMEMO (o GPG o OTR), le tue conversazioni saranno recapitate al destinatario senza che queste possano essere intercettate da nessuno (nemmeno dagli amministratori).

---

![](omemo_logo.png)
