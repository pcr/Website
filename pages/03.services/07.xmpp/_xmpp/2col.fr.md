---
title: XMPP
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<br><br><br><br>
<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">S'inscrire sur Disroot</a>
<a class="button button1" href="https://webchat.disroot.org/">Se connecter au chat web</a>

---
![](xmpp_logo.png?resize=128,128)

# XMPP
### Messagerie instantanée, décentralisée et fédérée

Communiquez en utilisant un protocole de chat standardisé, ouvert et fédéré, avec la possibilité de crypter votre communication avec le protocole OMEMO (basé sur la méthode de cryptage également utilisée par des services tels que Signal et Matrix). Avec XMPP, vous n'êtes pas lié à un fournisseur de services (par exemple, le serveur Disroot) mais vous pouvez communiquer librement avec des contacts d'autres [serveurs Jabber](https://compliance.conversations.im/), tout comme vous communiqueriez entre différents serveurs de messagerie.


Page d'accueil du projet: [https://xmpp.org](https://xmpp.org)
Serveur d'Implementation: [https://prosody.im](https://prosody.im)

<a href='https://compliance.conversations.im/server/disroot.org'><img src='https://compliance.conversations.im/badge/disroot.org'></a>
