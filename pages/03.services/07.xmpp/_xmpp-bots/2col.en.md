---
title: 'Xmpp Bots'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

Bots are chat accounts that will do things for you. From checking the weather, greeting newcomers, remembering things for you or even making web searches or supplying you with RSS feeds and ton of other useful and those more useless things. We offer number of multipurpose bots based on **hubot** as well as a dedicated bot server called `bot.disroot.org`. If you have a bot and you're looking for a home for it, get in touch with us.<br>

Our bot server does not keep any history on the server.  

---

![](hubot_logo.png)
