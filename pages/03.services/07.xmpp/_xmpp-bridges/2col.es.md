---
title: 'Puentes XMPP'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations.jpg)

---

## Pasarelas, Puentes y Transportes

XMPP permite varias maneras de conectar a diferentes protocolos de chat.

### Conectar a una sala IRC
Puedes conectarte a cualquier sala IRC en cualquier servidor a través de nuestra pasarela IRC Biboumi.
<ul class=disc>
<li>Para unirte a una sala IRC: <b><i>#sala%irc.dominio.tld@irc.disroot.org</i></b></li>
<li>Para agregar un contacto IRC: <b><i>nombredecontacto%irc.dominio.tld@irc.disroot.org</i></b></li>
</ul>

Donde ***#sala*** es la sala IRC a la cual quieres unirte e ***irc.dominio.tld*** es la dirección del servidor IRC en la que está hospedada. Asegúrate de dejar ***@irc.disroot.org*** como está, porque es la dirección de la pasarela IRC.

### Conectar a una sala Matrix
Puedes conectarte a cualquier sala Matrix hospedada en cualquier servidor Matrix a través del puente Matrix.org:
<ul class=disc>
<li>Para unirte a una sala Matrix: <b><i>#sala#matrix.dominio.ldt@matrix.org</i></b></li>
</ul>

Donde ***#sala*** es la sala Matrix a la que quieres unirte y **matrix.dominio.tld*** es la dirección del servidor Matrix que la hospeda. Asegúrate de dejar ***@matrix.org*** como está, porque es la dirección del puente Matrix.


### Lo que vendrá...
A futuro planeamos correr nuestro propio puente Matrix. También esperamos poder brindar un puente Telegram y un número de otros puentes y transportes.
