---
title: 'Participer à un sondage'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Participer au sondage
Choisissez vos préférences sans avoir besoin de créer un compte. Framadate permet une discussion approfondie au sein du sondage vous permettant de participer à la décision au-delà du simple fait de cocher votre choix préféré.

---

![](en/poll_participate.gif)
