---
title: 'Spazio extra per la posta elettronica'
bgcolor: '#FFF'
fontcolor: '#7A7A7A'
wider_column: left
---

## Aggiungi spazio di archiviazione per la posta elettronica

È possibile estendere l'archiviazione della posta elettronica fino a 10 GB al costo di 0,15 euro al GB al mese.

Per richiedere ulteriore spazio di archiviazione è necessario compilare questa [form](/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/it/forms/extra-storage-mailbox">Richiedi ulteriore spazio di archiviazione per la posta elettronica</a>
