---
title: 'Almacenamiento de Archivos'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Almacenamiento cifrado de archivos
Sube y comparte cualquier archivo. Todos los archivos almacenados están cifrados de extremo-a-extremo.



---
![](code.png)

## Editor de código colaborativo
Edita código junto a tus compañerxs de equipo mientras está cifrado de extremo-a-extremo con cero conocimiento por parte del servidor.
