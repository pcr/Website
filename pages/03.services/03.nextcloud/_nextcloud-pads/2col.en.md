---
title: 'Nextcloud Pads'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Collaborative pads


Create pads or spreadsheets directly from **Nextcloud** and keep track of them.

**Note! Pads and spreadsheets are part of another service and its content isn't stored in your cloud. Additionally pads can theoretically be accessed by anyone who can "guess" the random URL leading to them.**

---

![](en/OCownpad.png?lightbox=1024)
