---
title: 'Nextcloud Tasks'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Tasks

Organize, manage and sync your tasks across all devices.

---

![](en/OCtasks.png?lightbox=1024)
