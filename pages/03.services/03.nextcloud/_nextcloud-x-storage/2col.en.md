---
title: 'Cloud Storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Cloud Storage Space

With your **Disroot** account, you get 2GB FREE storage.
It is possible to extend your cloud storage from 2GB to 14, 29 or 54GB for the cost of 0.15 euro per GB per month. You can also choose to use some or all of the additional space for mail storage, please specify that in the comment section in the request form.

To request extra storage you need to fill in this [form](/forms/extra-storage-space).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/en/forms/extra-storage-space">Request Extra Storage</a>
