---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Ouvrir un pad</a>
<a class="button button1" href="https://calc.disroot.org/">Créer un tableau</a>

---

![](etherpad.png)
## Edition collaborative en temps réel
Les pads de Disroot sont alimentés par **Etherpad**. Un pad est un texte en ligne que vous pouvez éditer de manière collaborative, en temps réel, directement dans le navigateur web. Les modifications de chacun sont instantanément répercutées sur tous les écrans.

Rédigez des articles, des communiqués de presse, des listes de choses à faire, etc. avec vos amis, collègues ou étudiants.

Vous n'avez pas besoin d'un compte sur Disroot pour utiliser ce service.

Vous pouvez utiliser [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) sur Android pour ouvrir ou créer directement les pads Disroot sur votre appareil Android.


Disroot Pads : [https://pad.disroot.org](https://pad.disroot.org)

Page d'accueil du projet : [http://etherpad.org](http://etherpad.org)

Le code source : [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)


![](ethercalc.png)
## Feuille de calcul web collaboratif

Les feuilles de calcul de Disroot sont alimentées par **Ethercalc**. Vous pouvez créer une feuille de calcul en ligne que vous pouvez éditer en collaboration et en temps réel directement dans le navigateur Web. Les modifications de chacun sont instantanément répercutées sur tous les écrans.
Travaillez ensemble sur les inventaires, les formulaires de sondage, la gestion des listes, les sessions de brainstorming et plus encore !

Vous n'avez pas besoin d'un compte sur Disroot pour utiliser ce service.

Disroot Calc : [https://calc.disroot.org](https://calc.disroot.org)

Page d'accueil du projet : [https://ethercalc.net](https://ethercalc.net)

Le code source : [https://github.com/audreyt/ethercalc](https://github.com/audreyt/ethercalc)
