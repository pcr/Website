---
title: Blocs de notas
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Abre un bloc</a>
<a class="button button1" href="https://calc.disroot.org/">Crea una hoja de cálculo</a>

---

![](etherpad.png)
## Edición colaborativa en verdadero tiempo real
Los blocs de **Disroot** están desarrollados por **Etherpad**. Un bloc es un texto en línea que puedes editar colaborativamente, en tiempo real, directamente en tu navegador web. Los cambios de todxs se reflejan instantáneamente en todas las pantallas.

 Escribe artículos, comunicados de prensa, listados de pendientes, etc, junto con tus amigxs, compañerxs de estudio o colegas.

No necesitas una cuenta de **Disroot** para utilizar este servicio.

Puedes usar [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) en **Android** para abrir o crear directamente tus blocs de **Disroot** en tu dispositivo.

Blocs de Disroot: [https://pad.disroot.org](https://pad.disroot.org)

Página del proyecto: [http://etherpad.org](http://etherpad.org)

Código fuente: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)


![](ethercalc.png)
## Una hoja de cálculo web colaborativa.

Las hojas de cálculo de **Disroot** están desarrolladas por **EtherCalc**. Puedes crear hojas de cálculo en línea que puedes editar en forma colaborativa y en tiempo real directamente en el navegador web. Los cambios de todxs son reflejados instantáneamente en todas las pantallas.
¡Para trabajar juntxs en inventarios, formularios de encuesta, gestión de listas, sesiones de lluvia de ideas y más!

No necesitas una cuenta de **Disroot** para utilizar este servicio.

Calc de Disroot: [https://calc.disroot.org](https://calc.disroot.org)

Página del proyecto: [https://ethercalc.net](https://ethercalc.net)

Código fuente: [https://github.com/audreyt/ethercalc](https://github.com/audreyt/ethercalc)
