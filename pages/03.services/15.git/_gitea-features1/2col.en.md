---
title: 'Git Features'
wider_column: left
---

## Easy to use
Check the public repositories, find the one you want to work on and clone it on your computer. Or create your own! You can set it as a public or as a private repository.

## Fast and lightweight
Gitea offers very fast and light experience. No bloated, over-engineered feature set. Only things that are essential to collaborate on your open source project!

## Notifications
Receive emails when an issue was solved, a pull request created, etc.

## Secure
Use ssh key, two factor authentication and GnuPG to secure your repository.


---

![](en/gitea_explore.png?lightbox=1024)

![](en/gitea_secure.png?lightbox=1024)
